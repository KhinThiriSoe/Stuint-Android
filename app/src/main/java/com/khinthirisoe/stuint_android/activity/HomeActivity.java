package com.khinthirisoe.stuint_android.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.khinthirisoe.stuint_android.R;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements GestureDetector.OnGestureListener, View.OnClickListener {

    private int count = 0;
    private long startMillis = 0;

    GestureDetector gestureDetector;

    @BindView(R.id.rootView)
    RelativeLayout relativeRootView;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    @BindView(R.id.linearBoard)
    LinearLayout linearBoard;
    @BindView(R.id.linearQuestion)
    LinearLayout linearQuestion;

    @BindView(R.id.text_survey)
    TextView textSurvey;
    @BindView(R.id.text_feedback)
    TextView textFeedback;
    @BindView(R.id.edt_question)
    EditText edtQuestion;
    @BindView(R.id.edt_ans_a)
    EditText edtAnswerA;
    @BindView(R.id.edt_ans_b)
    EditText edtAnswerB;
    @BindView(R.id.edt_ans_c)
    EditText edtAnswerC;

    @BindView(R.id.edt_feedback)
    EditText edtFeedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        gestureDetector = new GestureDetector(HomeActivity.this, HomeActivity.this);

        textSurvey.setOnClickListener(this);

        textFeedback.setOnClickListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int eventAction = event.getAction();
        if (eventAction == MotionEvent.ACTION_UP) {

            long time = System.currentTimeMillis();

            if (startMillis == 0 || (time - startMillis > 1000)) {
                startMillis = time;
                count = 1;
            } else {
                count++;
            }

            if (count == 2) {
                linearLayout.setVisibility(View.VISIBLE);

                linearLayout.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (linearQuestion.getVisibility() == View.VISIBLE) {
                            getJsonFromString();
                        } else if (edtFeedback.getVisibility() == View.VISIBLE) {
                            String feedback = edtFeedback.getText().toString();
                            String getSurvey = getJsonFromString();

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(getSurvey);
                                jsonObject.put("feedback", feedback);

                                String post = jsonObject.toString();

                                Toast.makeText(HomeActivity.this, post, Toast.LENGTH_LONG).show();

                                edtFeedback.setVisibility(View.GONE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            linearBoard.setVisibility(View.VISIBLE);
                        }
                        return gestureDetector.onTouchEvent(event);
                    }
                });

            }

        }
        return super.onTouchEvent(event);
    }

    private String getJsonFromString() {
        String question = edtQuestion.getText().toString();
        String answerA = edtAnswerA.getText().toString();
        String answerB = edtAnswerB.getText().toString();
        String answerC = edtAnswerC.getText().toString();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("question", question);
            jsonObject.put("answerA", answerA);
            jsonObject.put("answerB", answerB);
            jsonObject.put("answerC", answerC);

            Toast.makeText(HomeActivity.this, jsonObject.toString(), Toast.LENGTH_LONG).show();

        } catch (JSONException exception) {
            exception.printStackTrace();
        }

        linearQuestion.setVisibility(View.GONE);
        return jsonObject.toString();

    }

    @Override
    public boolean onFling(MotionEvent motionEvent1, MotionEvent motionEvent2, float X, float Y) {

        // TODO Auto-generated method stub

        return true;
    }

    @Override
    public void onLongPress(MotionEvent arg0) {

        // TODO Auto-generated method stub

    }

    @Override
    public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {

        // TODO Auto-generated method stub

        return false;
    }

    @Override
    public void onShowPress(MotionEvent arg0) {

        // TODO Auto-generated method stub

    }

    @Override
    public boolean onSingleTapUp(MotionEvent arg0) {

        // TODO Auto-generated method stub

        return false;
    }

    @Override
    public boolean onDown(MotionEvent arg0) {

        // TODO Auto-generated method stub

        return false;
    }

    @Override
    public void onClick(View v) {

        int getId = v.getId();

        switch (getId) {

            case R.id.text_survey:
                linearQuestion.setVisibility(View.VISIBLE);
                break;
            case R.id.text_feedback:
                edtFeedback.setVisibility(View.VISIBLE);
            default:

                break;
        }
    }
}
